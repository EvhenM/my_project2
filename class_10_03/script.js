// const h1 = document.querySelector('h1');
// const h1_2 = document.querySelectorAll('h1')[0];
// const h1_3 = document.getElementsByTagName('h1')[0];
//
// console.log(h1 === h1_2, h1_2 === h1_3, h1 === h1_3);
//
// const a = documents.querySelector('.a');
// const a_2 = document.getElementsByTagName('a')[0];
//
// const id = document.getElementById('b');
// const id_2 = document.getElementById('#b');
// console.log(id, id_2, window.b); //// сомнительная штука

// h1.innerText = 'abc<p>pac</p>'
// h1.innerHTML += "abc"; // меняем текст += - добавляем текст
// // h1.style.backgroundColor = 'red';
//  h1.setAttribute('style', 'background-color:red')
// console.log(h1.style.fontSize);
//  const style = getComputedStyle(h1);
//  console.log(style.fontSize);

/*
const rows = document.querySelectorAll('tr:nth-child(odd');
rows.forEach(row => row.style.backgroundColor = 'red')*/


// const rows = document.getElementsByClassName('tr');
// // Array.from(rows);
// // new Array(rows);
// [...rows]
//     .fillter((r, i) => i % 2 ===0)
//     .
/////////////
// [...rows].forEach((row, i) => {
//     if(1 % 2 === 0) {
//         row.style.backgroundColor = 'red';
//     }
// }) ;


const rows = document.querySelectorAll('tr');
rows.forEach(row => {
    const emptyCellsCount = [...row.children]
        .filter(cell => !cell.innerText.trim())
        .length;
    const allEmpty = emptyCellsCount === row.children.length;
    const allFull = emptyCellsCount === 0;
    const notEmptyNorFull = !allEmpty && !allFull;
    if(allEmpty) {

        // row.className += ' all-empty'; // раньше так делали
        row.style.backgroundColor = 'red';
        return;
    }
    if(allFull) {
        row.style.backgroundColor = 'green';
        return;
    }
    if(notEmptyNorFull) {
        row.style.backgroundColor = 'orange'
    }
});